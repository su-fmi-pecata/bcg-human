const music = new function () {
    const measureParts = [3, 2, 2];
    const measurePartsLength = measureParts.reduce((accumulator, currentValue) => accumulator + currentValue);
    const timeFrames = [
        {
            length: 173.73863636363637,
            number: 79,
            rhythm: "wp5Te6I9GZo7bLK7Rsi17t3y7tI2y3VT",
            start: 1.7613636363636365,
            measures: [
                // [measure number, time]
                [0, 2],
                [8, 14.4],
                [16, 26.8],
                [24, 38.9],
                [32, 51],
                [40, 63.4],
                [48, 75.2],
                [56, 87.3],
                [64, 98.8],
                [72, 110.7],
                [80, 121.8],
                [88, 137]
            ]//.map(x => [x[0], x[1]*5])
        }
    ];

    this.barFromTime = function (time) {
        let previousNumberOfBars = 0;
        let i = 0;
        for (i = 0; i < timeFrames.length; i++) {
            const timeFrame = timeFrames[i];
            if (timeFrame.start + timeFrame.length >= time)
                break;
            previousNumberOfBars += timeFrame.number;
        }

        const measures = timeFrames[i].measures;
        time -= timeFrames[i].start;
        for (i = 0; i < measures.length; i++)
            if (measures[i][1] > time)
                break;

        if (i === 0)
            return {
                number: measures[0][0] + previousNumberOfBars,
                part: 0,
                partRound: 0
            };

        if (i >= measures.length)
            return {
                number: measures[measures.length - 1][0] + previousNumberOfBars,
                part: 0,
                partRound: 0
            };

        const k = (time - measures[i - 1][1]) / (measures[i][1] - measures[i - 1][1]);
        const measureNumber = measures[i - 1][0] * (1 - k) + measures[i][0] * k + previousNumberOfBars;
        const part = (measureNumber - Math.floor(measureNumber)) / (1 / measurePartsLength);
        const partRound = Math.floor(part);
        return {
            number: measureNumber,
            numberRound: Math.floor(measureNumber),
            part,
            partRound,
            partSmall: part-partRound
        };
    }
}();
