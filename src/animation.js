const animation = new function () {
    // рисувателно поле
    const renderer = new THREE.WebGLRenderer({ antialias: true });
    const scene = new THREE.Scene();
    const camera = new THREE.PerspectiveCamera(40, window.innerWidth / window.innerHeight, 0.1, 2000);

    const debug = true;
    const walking = true;
    const rotation = true;

    const degree = -30;

    const personDistanceScale = 70;
    const curveScale = 70;
    const curveStep = -.003;
    const rotationStep = .005;
    const curves = Object.freeze([
        Object.freeze({ x: 1 * curveScale, y: 0 * curveScale, z: 0 * curveScale }),
        Object.freeze({ x: 1 * curveScale, y: 0 * curveScale, z: 1 * curveScale }),
        Object.freeze({ x: 0 * curveScale, y: 0 * curveScale, z: 1 * curveScale }),
        Object.freeze({ x: -1 * curveScale, y: 0 * curveScale, z: 1 * curveScale }),
        Object.freeze({ x: -1 * curveScale, y: 0 * curveScale, z: 0 * curveScale }),
        Object.freeze({ x: -1 * curveScale, y: 0 * curveScale, z: -1 * curveScale }),
        Object.freeze({ x: 0 * curveScale, y: 0 * curveScale, z: -1 * curveScale }),
        Object.freeze({ x: 1 * curveScale, y: 0 * curveScale, z: -1 * curveScale })
    ]);

    const barLoop = 4;

    const people = [];
    this.pp = () => people;
    this.update = renderImage;

    function point(joint, x, y, z) {
        return scene.worldToLocal(joint.children[0].localToWorld(new THREE.Vector3(x, y, z)));
    }

    // region initialization and start
    function initLights() {
        // светлини
        const light = new THREE.DirectionalLight('lightblue', 1);
        light.position.set(0, 0, 4);
        scene.add(light.clone());
        light.position.set(0, 0, -4);
        scene.add(light.clone());
        scene.add(new THREE.AmbientLight('white', 0.5));
    }

    function initPeople() {
        function setPosition(number, p) {
            p.position.fromStart = number * curveStep;
            p.position.curveNum = 0;
            p.rotateY((4 / 8) * Math.PI);
            Object.assign(p.position, calculateNextPoint(p.position));
            p.r.leg.rotate(0, 0, degree);
            p.r.knee.rotate(0, 0, -2 * degree);
            p.position.rotation = 0;
            hideJoint(p.pelvis);

            const dressColor = new THREE.MeshPhongMaterial({ color: 'white', shininess: 200 });
            const dress = new THREE.Mesh(
                new THREE.CylinderGeometry(6, 7, 19, 16),
                dressColor
            );
            dress.position.y = 1.5;
            addToJoint(p.body, dress.clone());
        }

        const adam = function (number) {
            const p = male();
            setPosition(number, p);

            const hat = new THREE.Mesh(
                new THREE.ConeGeometry(4, 10, 32),
                new THREE.MeshPhongMaterial({ color: 'black', shininess: 10 })
            );
            hat.rotateY(-Math.PI);
            hat.position.y = 9;
            addToJoint(p.head, hat.clone());

            const generateSphere = (radius) => new THREE.SphereGeometry(radius, 64, 64);

            function trousres(side) {
                const magicRadiusMiddle = 2.5;
                const magicRadiusTop = 2.7;
                const ternColor = new THREE.MeshPhongMaterial({ color: '#FBFCC7', shininess: 10 });

                const top = new THREE.Mesh(
                    generateSphere(magicRadiusTop),
                    ternColor
                );
                addToJoint(side.leg, top);

                const ternTop = new THREE.Mesh(
                    new THREE.CylinderGeometry(magicRadiusMiddle, magicRadiusTop, 14, 32),
                    ternColor
                );
                ternTop.position.y = 7.5;
                addToJoint(side.leg, ternTop.clone());
                hideJoint(side.leg);

                const ternMiddle = new THREE.Mesh(
                    generateSphere(magicRadiusMiddle + 0.07),
                    ternColor
                );
                addToJoint(side.knee, ternMiddle);

                const ternBottom = new THREE.Mesh(
                    new THREE.CylinderGeometry(1.5, magicRadiusMiddle, 12, 32),
                    ternColor
                );
                ternBottom.position.y = 6.7;
                addToJoint(side.knee, ternBottom.clone());
                hideJoint(side.knee);
            }

            trousres(p.l);
            trousres(p.r);
            return p;
        };

        const eva = function (number) {
            const p = female();
            setPosition(number, p);
            const bracelet = new THREE.Mesh(
                new THREE.CylinderGeometry(2, 2, 1, 16),
                new THREE.MeshPhongMaterial({ color: 'red', shininess: 200 })
            );
            bracelet.position.y = 6;
            addToJoint(p.l.elbow, bracelet.clone());
            addToJoint(p.r.elbow, bracelet.clone());

            return p;
        };

        if (debug) {
            people.push(adam(0));
            people.push(eva(personDistanceScale));
        } else {
            for (let i = 0; i < 2; i++) {
                people.push(adam(i * personDistanceScale));
            }
            for (let i = 0; i < 2; i++) {
                people.push(eva((i + 2) * personDistanceScale));
            }
        }

        console.log(people);
        scene.add(...people);
    };

    function initBackground() {

    };

    this.init = function () {
        // init render
        renderer.setSize(window.innerWidth, window.innerHeight);
        document.body.appendChild(renderer.domElement);
        renderer.domElement.classList.add('fullScreen');

        // сцена и камера
        scene.background = new THREE.Color('ghostwhite');
        camera.position.set(0, 0, 150);

        initLights();
        initPeople();
        initBackground();
        renderImage();
    };

    this.start = function () {
        AudioPlayer.play();
        drawFrame();
    };


    function renderImage() {
        renderer.render(scene, camera);
    }

    // функция за анимиране на сцената
    function drawFrame() {
        requestAnimationFrame(drawFrame);
        if (animate) animate(AudioPlayer.currentTime);
        renderImage();
    }

    // endregion

    // SCENE Dimensions
    // x: [-140, 140]
    // y: [  -5, 9]

    function calculateNextPoint(position, directionScale = 1, step = curveStep) {
        function setAndCalc(cord, res, p0, p1, p2) {
            res[cord] = calc(res.fromStart, p0[cord], p1[cord], p2[cord]);
            // console.log("res: " + JSON.stringify(res));
        }

        function calc(t, x0, x1, x2) {
            return (1 - t) * (1 - t) * x0 + 2 * (1 - t) * t * x1 + t * t * x2;
        }

        const res = JSON.parse(JSON.stringify(position));
        res.fromStart = position.fromStart + directionScale * step;

        if (res.fromStart > 1) {
            res.fromStart -= 1;
            res.curveNum = (res.curveNum + 2) % curves.length;
        } else if (res.fromStart < 0) {
            res.fromStart += 1;
            res.curveNum = (res.curveNum - 2 + curves.length) % curves.length;
        }
        const p0 = curves[(res.curveNum + 0) % curves.length];
        const p1 = curves[(res.curveNum + 1) % curves.length];
        const p2 = curves[(res.curveNum + 2) % curves.length];
        setAndCalc("x", res, p0, p1, p2);
        setAndCalc("y", res, p0, p1, p2);
        setAndCalc("z", res, p0, p1, p2);
        return res;
    }


    function forwardStep(measure, person, master, slave) {
        const pm = person[master];
        const ps = person[slave];
        // const step = 2;
        // const degreePart = -17;
        // const chukcheScale = 11;
        // const chukcheMaxY = 0.5;
        const part = measure.partSmall;
        const prev = 1 - part; // pReverse not previous
        const rot = degree * part;
        const rev = degree * prev;
        if (rotation) person.rotateY(rotationStep);
        switch (measure.partRound) {
            // region RAAAZ
            case 0:
                // back rotation
                if (person.position.rotation != 0) {
                    const newAngle = -(Math.PI / 4) * prev;
                    person.rotateY(person.position.rotation - newAngle);
                    person.position.rotation = newAngle;
                }

                ps.ankle.rotate(0, 0, -rot / 3);
                ps.leg.rotate(0, 0, -rot / 6);
                pm.knee.rotate(0, 0, -2 * rev);
                if (walking) Object.assign(person.position, calculateNextPoint(person.position));
                break;
            case 1:
                // back to normal position
                if (person.position.rotation !== 0) {
                    person.rotateY(person.position.rotation);
                    person.position.rotation = 0;
                }
                ps.ankle.rotate(0, 0, -rev / 3);
                ps.knee.rotate(0, 0, -rot);
                ps.leg.rotate(0, 0, -degree / 3 - rot / 6);
                if (walking) Object.assign(person.position, calculateNextPoint(person.position));
                break;
            case 2:
                ps.knee.rotate(0, 0, -degree - rot);
                ps.leg.rotate(0, 0, /*-rev/3+*/rot);
                pm.leg.rotate(0, 0, rev);
                if (walking) Object.assign(person.position, calculateNextPoint(person.position));
                break;
            // endregion RAAAZ
            //region DVA
            case 3:
                // ps.leg.rotate(0, 0, rot);
                ps.knee.rotate(0, 0, -2 * rev);
                pm.knee.rotate(0, 0, -rot);
                pm.leg.rotate(0, 0, -rot / 3);
                if (walking) Object.assign(person.position, calculateNextPoint(person.position));
                break;
            case 4:
                ps.leg.rotate(0, 0, rev);
                pm.knee.rotate(0, 0, -degree - rot);
                pm.leg.rotate(0, 0, -rev / 3 + rot);
                if (walking) Object.assign(person.position, calculateNextPoint(person.position));
                break;
            //endregion DVA
            //region TRI
            case 5:
                ps.knee.rotate(0, 0, -rot);
                ps.leg.rotate(0, 0, -rot / 3);
                pm.knee.rotate(0, 0, -2 * rev);
                break;
            case 6:
                ps.knee.rotate(0, 0, -degree - rot);
                ps.leg.rotate(0, 0, -rev / 3 + rot);
                pm.leg.rotate(0, 0, rev);
                break;
            //endregion TRI
        }
    }

    function backwardStep(measure, person) {
        const pos = person.position;
        const pm = person['r'];
        const ps = person['l'];
        const chukcheMaxY = 0.5;
        const part = measure.partSmall;
        const prev = 1 - part; // pReverse not previous
        const rot = degree * part;
        const rev = degree * prev;
        switch (measure.partRound) {
            // region RAAAZ
            case 0:
                ps.ankle.rotate(0, 0, -rot);
                Object.assign(pos, { y: pos.y + chukcheMaxY * part });
                break;
            case 1:
                ps.ankle.rotate(0, 0, -rev);
                ps.knee.rotate(0, 0, -2 * rot);
                ps.leg.rotate(0, 0, rot);
                pm.leg.rotate(0, 0, -rot / 3);
                Object.assign(pos, { y: pos.y + chukcheMaxY * 2 * (prev - 0.5) });
                break;
            case 2:
                pm.knee.rotate(0, 0, -2 * rev);
                pm.leg.rotate(0, 0, -rev / 3);
                ps.leg.rotate(0, 0, degree + rot);
                if (walking) Object.assign(person.position, calculateNextPoint(person.position, -1));
                Object.assign(pos, { y: pos.y + chukcheMaxY * part });
                break;
            // endregion RAAAZ
            //region DVA
            case 3:
                pm.leg.rotate(0, 0, rot / 3);
                ps.knee.rotate(0, 0, -2 * rev);
                ps.leg.rotate(0, 0, 2 * rev - rot/*/3*/);
                break;
            case 4:
                pm.knee.rotate(0, 0, -2 * rot);
                pm.leg.rotate(0, 0, rot);
                ps.leg.rotate(0, 0, -rev);
                if (walking) Object.assign(person.position, calculateNextPoint(person.position, -1));
                break;
            //endregion DVA
            //region TRI
            case 5:
                pm.knee.rotate(0, 0, -2 * rev);
                pm.leg.rotate(0, 0, rev - rot);
                ps.leg.rotate(0, 0, rot / 3);
                break;

            case 6:
                pm.leg.rotate(0, 0, -rev);
                ps.knee.rotate(0, 0, -2 * rot);
                ps.leg.rotate(0, 0, rot);
                if (walking) Object.assign(person.position, calculateNextPoint(person.position, -1));
                break;
            //endregion TRI
        }
    }


    function crossStep(measure, person) {
        const pos = person.position;
        const pm = person['r'];
        const ps = person['l'];
        const chukcheMaxY = 0.5;
        const part = measure.partSmall;
        const prev = 1 - part; // pReverse not previous
        const rot = degree * part;
        const rev = degree * prev;
        switch (measure.partRound) {
            // region RAAAZ
            case 0:
                const newAngle = -(Math.PI / 4) * part;
                person.rotateY(person.position.rotation - newAngle);
                person.position.rotation = newAngle;
                break;
            case 1:
                // ps.ankle.rotate(0, 0, -rev);
                // ps.knee.rotate(0, 0, -2 * rot);
                // ps.leg.rotate(0, 0, rot);
                // pm.leg.rotate(0, 0, -rot / 3);
                // Object.assign(pos, { y: pos.y + chukcheMaxY * 2 * (prev - 0.5) });
                break;
            case 2:
                // pm.knee.rotate(0, 0, -2 * rev);
                // pm.leg.rotate(0, 0, -rev / 3);
                // ps.leg.rotate(0, 0, degree + rot);
                // Object.assign(pos, { y: pos.y + chukcheMaxY * part });
                break;
            // endregion RAAAZ
            //region DVA
            case 3:
                // pm.leg.rotate(0, 0, rot / 3);
                // ps.knee.rotate(0, 0, -2 * rev);
                // ps.leg.rotate(0, 0, 2 * rev - rot/*/3*/);
                break;
            case 4:
                // pm.knee.rotate(0, 0, -2 * rot);
                // pm.leg.rotate(0, 0, rot);
                // ps.leg.rotate(0, 0, -rev);
                break;
            //endregion DVA
            //region TRI
            case 5:
                // pm.knee.rotate(0, 0, -2 * rev);
                // pm.leg.rotate(0, 0, rev - rot);
                // ps.leg.rotate(0, 0, rot / 3);
                break;

            case 6:
                // pm.leg.rotate(0, 0, -rev);
                // ps.knee.rotate(0, 0, -2 * rot);
                // ps.leg.rotate(0, 0, rot);
                break;
            //endregion TRI
        }
    }

    // анимация на човечето
    function animate(t) {

        const measure = music.barFromTime(t);
        if (measure.number === 0 && measure.part < 0.000001) return;
        let manipulator = undefined;
        switch (measure.numberRound % barLoop) {
            case 0:
                manipulator = (measure, p) => forwardStep(measure, p, 'r', 'l');
                break;
            case 1:
                manipulator = (measure, p) => forwardStep(measure, p, 'l', 'r');
                break;
            case 2:
                manipulator = backwardStep;
                break;
            case 3:
                manipulator = crossStep;
                break;
        }

        if (manipulator) people.forEach(p => manipulator(measure, p));

    }
}();
